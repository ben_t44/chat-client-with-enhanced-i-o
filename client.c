/* Chat client for exploring the use of network dunctions in C
 * Written by Ben Thomas and John Rodkey, based on original concepts by Thomas Cantrell
 *
 * Assised by James Bek and Dempsey Salazar
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <errno.h>
#include <sys/select.h>
#include <ncurses.h>
#include <pthread.h>

#define SERVER "10.115.20.250"
#define PORT 49153
#define BUFSIZE 1024
#define STDIN 0

// Global variables: outout window, input window, and file descriptor
WINDOW *output;
WINDOW *input;
int FD;

/* Create a TCP connection to a server on the designated port,
 * return a file descriptor to the socket. UNCHANGED*/
int connect2v4stream(char * srv, int port){
	int ret, sockd;
	struct sockaddr_in sin;

	if ( (sockd = socket(AF_INET, SOCK_STREAM, 0)) == -1 ){
		endwin();
		printf("ERROR: Error creating socket. errno = %d\n", errno);
		exit(errno);
	}
	if ( (ret = inet_pton(AF_INET, SERVER, &sin.sin_addr)) <= 0 ){
		endwin();
		printf("ERROR: Trouble converting using inet_pton. \
			return value = %d, errno = %d\n", ret, errno);
		exit(errno);
	}
	sin.sin_family = AF_INET; /* IPv4 */
	sin.sin_port = htons(PORT); /* Convert port to network endian */
	
	if ( (connect(sockd, (struct sockaddr *) &sin, sizeof(sin))) == -1){
		endwin();
		printf("ERROR: Trouble connecting to server. errno = %d\n", errno);
		exit(errno);
	}
	return sockd;
}


/* Send a string to a socket when user presses enter, exits 
 * when user enters 'quit'. Runs on pthread 'sendTH' */
void* sendout(){

	char *buff, *origbuffer;
	int ret;
	int len;
	while(1){
		// move cursor to input window	
		wmove(input, 1, 0);
		// reset buffer for input
		len = BUFSIZE;
		buff = malloc(len + 1);
		origbuffer = buff;
		// get user input
		wgetstr(input, buff);
		strcat(buff, "\n");
		// if user enters 'quit' program exits
		if (strcmp (buff, "quit\n") == 0 ){
			endwin();
			exit(1);
		}
		// send user input + Exception
		ret = send(FD, buff, strlen(buff), 0);
		if (ret == -1){
			endwin();
			printf("ERROR: Trouble sending. errno = %d\n", errno);
			exit(errno);
		}
		// setup for next iteration
		wclear(input);
		wrefresh(input);
		free(origbuffer);
	}	
}

/* Receive and print strings from socket to output window
 * until there is no more input on the socket. Runs 
 * on pthread 'receiveTH' */
void* recvandprint(){

	char *buff, *origbuffer;
	int ret;
	while(1){
		// setup select() + Exception
		fd_set rfds;
		FD_ZERO(&rfds);
		FD_SET(FD, &rfds);
		if (select(FD+1, &rfds, NULL, NULL, NULL) < 0){
			endwin();
			printf("ERROR: Trouble selecting. errno = %d\n", errno);
			exit(errno);
		}
		
		// reset buffer for incomming messages
		buff = malloc(BUFSIZE + 1);
		origbuffer = buff;
		
		// receive message from server + Exception
		ret = recv(FD, buff, BUFSIZE,0);
		if (ret == -1){
			endwin();
			printf("ERROR: Error receiving. errno = %d\n", errno);
			exit(errno);
		}
		// print message to output window
		else {
			buff[ret] = 0;
			wprintw(output, buff);
		}
		// setup for next iteration
		free(origbuffer);
		wrefresh(output);
	}
}

/* Initializes and formats the input and output ncurses windows, 
 * as well as the asthetic box around the input window */ 
void make_windows(){
	WINDOW *box;
	// enable ncurses environment
	initscr();
	raw();

	// create output window
	output = newwin(15, COLS, 0, 0);
	scrollok(output, TRUE);

	// create input window
	input = newwin(3, COLS-2, 17, 1);
	scrollok(input, TRUE);

	// create asthetic box
	box = newwin(5, COLS, 16, 0);
	box(box, 0, 0);
	wprintw(box, "Type 'quit' to exit");
	wrefresh(box);
}

void main(int argc, char * argv[]){
	int len;
	char *name;
	void *foo;
	pthread_t receiveTH;
	pthread_t sendTH;

	// create screen windows
	make_windows();
	
	// connect to server
	FD = connect2v4stream( SERVER, PORT );


	// set name based on arguments
	if(argv[1] == NULL){ // check for name
		endwin();
		printf("Usage: client <screenname>\n");
		exit(1);
	}
	name = argv[1];
	len = strlen(name);
	name[len] = '\n';  // insert \n 
	name[len + 1] = '\0'; // reterminate the string 
	send(FD, name, strlen(name), 0);// send name to server
	
	// create threads
	pthread_create(&receiveTH, NULL, recvandprint, NULL);// receiveTH , recvandprint()
	pthread_create(&sendTH, NULL, sendout, NULL);// sendTH, senout()

	// start threads
	pthread_join(receiveTH, NULL);
	pthread_join(sendTH, NULL);
}
